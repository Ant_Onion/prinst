package stack

var stack []float64

// Push a new value to the stack.
func Push (n float64) {
	stack = append (stack, n)
}

// Pop and return the value on the top of the stack.
func Pop() float64 {
	var pop float64

	pop, stack = stack[len (stack) - 1], stack[:len (stack) - 1]

	return pop
}

// Add the top of the stack to the previous number.
func Add() {
	Push (Pop() + Pop())
}

// Subtract the top of the stack to the previous number.
func Sub() {
	Push (Pop() - Pop())
}

// Multiply the top of the stack to the previous number.
func Mul() {
	Push (Pop() * Pop())
}

// Divide the top of the stack to the previous number.
func Div() {
	Push (Pop() / Pop())
}

// Drop the top value.
func Drop() {
	Pop()
}

// Swap the top of the stack for the number before.
func Swap() {
	var n1, n2 = Pop(), Pop()

	Push(n1)
	Push(n2)
}

// n3 n2 n1 --- n2 n1 n3
func Rot() {
	var n1, n2, n3 = Pop(), Pop(), Pop()

	Push (n2)
	Push (n1)
	Push (n3)
}

// [[[ DEBUG ]]]
// Return the entire stack without popping anything.
func RetStack() []float64 {
	return stack
}
