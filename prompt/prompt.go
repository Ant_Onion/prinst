package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"rpn.com/interpreter"
	// "rpn.com/stack"
)

func main() {
	var output string = ""
	reader := bufio.NewReader (os.Stdin)

	// TODO Put a condition here.
	for true {
		fmt.Printf (">>> ")
		line, err := reader.ReadString ('\n')
		if err == nil {
			output = interpreter.Interpret (strings.Trim (line, "\n"))
			if output == "" {
				// GOTO the end of the function.
				goto Final
			}
			fmt.Printf (output)
		}
	}
Final:
}
