module prompt

go 1.15

replace rpn.com/stack => ../stack

replace rpn.com/interpreter => ../interpreter

require (
	rpn.com/interpreter v0.0.0-00010101000000-000000000000
	rpn.com/stack v0.0.0-00010101000000-000000000000
)
