package interpreter

import (
	// "bytes"
	"fmt"
	// "log"
	"strconv"
	"strings"
	"os"
	"os/exec"

	"rpn.com/stack"
)

// Return TRUE if the word is a number.
func isNumber (word string) bool {
	var ret bool = true

	_, err := strconv.ParseFloat (word, 32)
	if err != nil {
		ret = false
	}

	return ret
}

// Concatenates the top of the stack into the return string.
func printTop (ret *string) {
	// fmt.Printf ("Show top of the stack\n")
	*ret += fmt.Sprintf ("%.2f ", stack.Pop()) + "\n"
}

func printStack (ret *string) {
	// fmt.Printf ("Show the entire stack\n")
	slice := stack.RetStack()
	for _, n := range slice {
		*ret += fmt.Sprintf ("%.2f ", n)
	}
	*ret += "\n"
}

// List the files in the current directory
// func lsCmd (arguments []string, ret *string) {
	// DEBUG
	// for _, w := range arguments {
		// fmt.Printf ("%s ", w)
	// }
//
	// if len (arguments) == 0 {
		// files, err := os.ReadDir (".")
		// if err != nil {
			// log.Fatal (err)
		// }
//
		// for _, file := range files {
			// *ret += fmt.Sprintf ("%s ", file.Name())
		// }
	// } else {
		// for _, arg := range arguments {
			// files, err := os.ReadDir (arg)
			// if err != nil {
				// log.Fatal (err)
			// }
//
			// for _, file := range files {
				// *ret += fmt.Sprintf ("%s ", file.Name())
				// fmt.Println ("")
			// }
		// }
	// }
// }

func echo (arguments []string, ret *string) {
	for _, arg := range arguments {
		*ret += arg
	}
	*ret += "\n"
}

// NOTE: Shell features don't work with exec.Command() like '~'.
func execCommand (name string, ret *string) {
	home, _ := os.LookupEnv ("HOME")
	var pisbin string = home + "/Programacion/Go/prinst/pisbin/"
	// var pissrc string = "~/Programacion/Go/prinst/pissrc/"

	cmd := exec.Command (pisbin + name, "../")
	// var output bytes.Buffer
	// cmd.Stdout = &output
	// err := cmd.Run()
	out, err := cmd.Output()
	if err != nil {
		// fmt.Printf ("Command '"+ name +"' not found")
		*ret += "Command '"+ name +"' not found"
	} else {
		// fmt.Printf ("%q ", string (out))
		*ret += string (out)
	}
	*ret += "\n"
}

// Interpret the string recieved as parameter.
func Interpret (input string) string {
	var arrInput = strings.Split (input, " ") // INPUT
	var ret string = "" // Return OUTPUT
	var args bool = false
	var arguments []string

	// Check every word of the input and check if is a number
	// or a string.
	for _, word := range arrInput {
		if args {
			if word != ")" {
				arguments = append (arguments, word)
			} else {
				args = false
			}
		} else {
			// If the word is a number, is stored in the stack.
			if isNumber (word) {
				fmt.Printf ("[ DEBUG ] Number: %s\n", word)
				n, _ := strconv.ParseFloat (word, 32)
				stack.Push (n)
			// If the word isn't a number, is checked if is any special word.
			} else {
				fmt.Printf ("[ DEBUG ] Word: %s\n", word)
				switch word {
					// GOTO the end of the function.
					case "exit": goto Final

					// Number operators
					case "+": stack.Add()
					case "-": stack.Sub()
					case "*": stack.Mul()
					case "/": stack.Div()

					// Print
					case ".": printTop (&ret)
					case ".s": printStack (&ret)
					case "echo": echo (arguments, &ret)
					case "print": echo (arguments, &ret)

					// Stack management
					case "swap": stack.Swap()
					case "rot": stack.Rot()
					case "drop": stack.Drop()

					// Command arguments
					case "(":
						args = true
						// Clear the slice
						arguments = nil

					// External commands
					default: execCommand (word, &ret)
				}
			}
		}
	}

	// Final concatenation before return.
	ret += "-- done --\n"
Final:
	return ret
}
